#!/bin/bash

##### CONFIGURATION #####

source ../lib/lib_logging.sh

output_log=${0}_$(date +"%Y%m%d_%H%M%S").log

setVerboseOn
setConsoleLogLevel DEBUG
setFileLogLevel INFO
setOutputLog ${output_log}
setDieOn

##### FUNCTIONS #####

function install_pkg {
    LOG_INFO "Installing ${1} "
    sudo apt install -y ${1} >> ${output_log}
    if [[ "$?" == "0" ]]; then
        LOG_INFO "${1} installed"
    else
        DIE "Error while installing: ${1}"
    fi
}

function write_file {
    if [[ "${1}" == "-o" ]]; then
        shift
        local of=${1}
        shift
        LOG_INFO "Writing: ${@} > ${of}"
        echo ${@} > ${of}
    else
        local of=${1}
        shift
        LOG_INFO "Writing: ${@} >> ${of}"
        echo ${@} >> ${of}
    fi
}

function makedir {
    if [[ ! -e $1 ]]; then
        mkdir $1
    else
        LOG_WARNING '$1 already exists'
    fi
}

##### MAIN SCRIPT #####

if [ "$(id -u)" -ne "0" ]; then
    LOG_ERROR "Script must be run as root"
    exit 1
fi

LOG_INFO "Installing new packages"
install_pkg vim

LOG_INFO "Configuring global profile"
gpf=/etc/profile.d/personal.sh
LOG_DEBUG "Global profile: ${gpf}"
write_file -o ${gpf} '# Personal global bash profile'
write_file ${gpf} 
write_file ${gpf} '# new aliases'
write_file ${gpf} "ls='ls --color=auto'"
write_file ${gpf} "la='ls -lah'"
write_file ${gpf} "ll='ls -lh'"
write_file ${gpf} "lrt='ls -lrth'"
write_file ${gpf} "lash='ls -lash'"
write_file ${gpf} "grep='grep --color=auto'"
write_file ${gpf} 'vi=vim'
write_file ${gpf} 
write_file ${gpf} '# new PATH'
write_file ${gpf} 'PATH=$PATH:/tools/bin'

LOG_INFO "Adding color to pi user"
pi_bashrc=/home/pi/.bashrc
LOG_DEBUG "pi bashrc: ${pi_bashrc}"
sed -i_$(date +"%Y%m%d_%H%M%S") 's/(^PS[0-9].*)/#\1/g' ${pi_bashrc}
write_file ${pi_bashrc} 'PS1='"'"'${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\W \$\[\033[00m\] '"'"
write_file ${pi_bashrc} 'PS2='"'"'> '"'"

LOG_INFO "Adding color to root user"
root_bashrc=/root/.bashrc
LOG_DEBUG "root bashrc: ${root_bashrc}"
sed -i_$(date +"%Y%m%d_%H%M%S") 's/(^PS[0-9].*)/#\1/g' ${root_bashrc}
write_file ${root_bashrc} 'PS1='"'"'${debian_chroot:+($debian_chroot)}\[\033[01;31m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\W \$\[\033[00m\] '"'"
write_file ${root_bashrc} 'PS2='"'"'> '"'"

LOG_INFO "Configuring logrotate crontab"
if [[ -e /etc/crontab ]]; then
    cp /etc/crontab /etc/crontab_$(date +"%Y%m%d_%H%M%S")
    sed -i 's/[ \t]\{1,\}/ /g' /etc/crontab
    sed -i 's/^\([*0-9]\{1,3\} \)\{5\}\(root.*cron.hourly.*\)$/0 * * * * \2/g' /etc/crontab
    sed -i 's/^\([*0-9]\{1,3\} \)\{5\}\(root.*cron.daily.*\)$/0 0 * * * \2/g' /etc/crontab
    sed -i 's/^\([*0-9]\{1,3\} \)\{5\}\(root.*cron.weekly.*\)$/0 0 * * 7 \2/g' /etc/crontab
    sed -i 's/^\([*0-9]\{1,3\} \)\{5\}\(root.*cron.monthly.*\)$/0 0 1 * * \2/g' /etc/crontab
fi

LOG_INFO 'Making tools directories'
makedir /tools
makedir /tools/app
makedir /tools/lib
makedir /tools/bin

chmod -R 755 /tools
chown pi:pi /tools/app
chmod +s /tools/app

LOG_INFO "End of ${0##*/} script"
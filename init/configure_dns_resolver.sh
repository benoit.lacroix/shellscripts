#!/bin/bash

if [ "$(id -u)" -ne "0" ]; then
	echo "Script must be run as root"
	exit 1
fi

dns_srv=192.168.1.81

cur_file=/etc/resolv.conf
old_file=/etc/resolv.conf.old

if [ "$(grep "^nameserver" ${cur_file})" -eq "nameserver ${dns_srv}" ]; then
	echo "DNS resolution already configured to ${dns_srv}"
	exit 0
fi

# backup the current file
mv ${cur_file} ${old_file}

# toggle comment to all active lines
while read line; do sed 's/^nameserver/#nameserver/g' >> ${cur_file}; done < ${old_file}

# adding new dns resolution server
echo nameserver ${dns_srv} >> ${cur_file}

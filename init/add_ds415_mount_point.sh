#!/bin/bash

if [ "$(id -u)" -ne "0" ]; then
	echo "Script must be run as root"
	exit 1
fi

orig_server=ds415.nas
orig_directory=${orig_server}:/volume1/home_nas

dest_point=/tools/nas/

if [[ ! -z "$(ls ${dest_point})" ]]; then
	echo "NAS already mounted"
	exit 0
fi

echo "Mounting ${orig_directory} to ${dest_point}"
mount -o nolock ${orig_directory} ${dest_point}
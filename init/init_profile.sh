#!/bin/bash

##### CONFIGURATION #####

source ../lib/lib_logging.sh

output_log=${0}_$(date +"%Y%m%d_%H%M%S").log

setVerboseOn
setConsoleLogLevel DEBUG
setFileLogLevel INFO
setOutputLog ${output_log}
setDieOn

##### FUNCTIONS #####

function install_pkg {
    LOG_INFO "Installing ${1} "
    sudo apt install -y ${1} >> ${output_log}
    if [[ "$?" == "0" ]]; then
        LOG_INFO "${1} installed"
    else
        DIE "Error while installing: ${1}"
    fi
}

function write_file {
    if [[ "${1}" == "-o" ]]; then
        shift
        local of=${1}
        shift
        LOG_INFO "Writing: ${@} > ${of}"
        echo ${@} > ${of}
    else
        local of=${1}
        shift
        LOG_INFO "Writing: ${@} >> ${of}"
        echo ${@} >> ${of}
    fi
}

function makedir {
	if [[ ! -e $1 ]]; then
		mkdir $1
	else
		LOG_WARNING '$1 already exists'
	fi
}

##### MAIN SCRIPT #####

if [ "$(id -u)" -ne "0" ]; then
    LOG_ERROR "Script must be run as root"
    exit 1
fi

LOG_INFO "Installing new packages"
install_pkg vim

LOG_INFO "Configuring DNS Resolver"
dns_resolv=/etc/NetworkManager/system-connections/personal-dsl
LOG_DEBUG "DNS Configuration: ${dns_resolv}"
write_file -o ${dns_resolv} '[ipv4]'
write_file ${dns_resolv} 'method=auto'
write_file ${dns_resolv} 'dns=192.168.1.81;'
write_file ${dns_resolv} 'ignore-auto-dns=true'

LOG_INFO "Configuring global profile"
gpf=/etc/profile.d/personal.sh
LOG_DEBUG "Global profile: ${gpf}"
write_file -o ${gpf} '# Personal global bash profile'
write_file ${gpf} 
write_file ${gpf} '# new aliases'
write_file ${gpf} "ls='ls --color=auto'"
write_file ${gpf} "la='ls -lah'"
write_file ${gpf} "ll='ls -lh'"
write_file ${gpf} "lrt='ls -lrth'"
write_file ${gpf} "lash='ls -lash'"
write_file ${gpf} "grep='grep --color=auto'"
write_file ${gpf} 'vi=vim'
write_file ${gpf} 
write_file ${gpf} '# new PATH'
write_file ${gpf} 'PATH=$PATH:/tools/bin'

LOG_INFO "Adding color to benoit user"
benoit_bashrc=/home/benoit/.bashrc
LOG_DEBUG "benoit bashrc: ${benoit_bashrc}"
sed -i_$(date +"%Y%m%d_%H%M%S") 's/(^PS[0-9].*)/#\1/g' ${benoit_bashrc}
write_file ${benoit_bashrc} 'PS1='"'"'${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\W \$\[\033[00m\] '"'"
write_file ${benoit_bashrc} 'PS2='"'"'> '"'"

LOG_INFO "Adding color to root user"
root_bashrc=/root/.bashrc
LOG_DEBUG "root bashrc: ${root_bashrc}"
sed -i_$(date +"%Y%m%d_%H%M%S") 's/(^PS[0-9].*)/#\1/g' ${root_bashrc}
write_file ${root_bashrc} 'PS1='"'"'${debian_chroot:+($debian_chroot)}\[\033[01;31m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\W \$\[\033[00m\] '"'"
write_file ${root_bashrc} 'PS2='"'"'> '"'"

LOG_INFO 'Making tools directories'
makedir /tools
makedir /tools/app
makedir /tools/lib
makedir /tools/bin

chmod -R 755 /tools
chown benoit:benoit /tools/app
chmod +s /tools/app

LOG_INFO "End of ${0##*/} script"
#!/bin/bash

if [ "$(id -u)" -ne "0" ]; then
    echo "Script must be run as root"
    exit 1
fi

influxdb_host=192.168.1.80
influxdb_port=8086
influxdb_db=telegraf
influxdb_user=
influxdb_pwrd=

telegraf_version=1.4.4
telegraf_archive=telegraf-${telegraf_version}_linux_armhf.tar.gz

telegraf_conf=/etc/telegraf/telegraf.conf

if [[ -e /urs/bin/telegraf ]]; then
    echo "Telegraf already installed"
    exit 1
else
    echo "Creating telegraf user"
    useradd telegraf

    echo "Downloading telegraf version: ${telegraf_version}"
    wget https://dl.influxdata.com/telegraf/releases/${telegraf_archive}

    echo "Unarchiving telegraf..."
    tar xf ${telegraf_archive}

    echo "Copying files..."
    cd telegraf

    cp -r etc/ /
    cp -r usr/ /
    cp -r var/ /
    cp /usr/lib/telegraf/scripts/telegraf.service /lib/systemd/system/

    echo "Configuring telegraf"
    telegraf --output-filter influxdb config > ${telegraf_conf}

    sed -i 's#\["http://localhost:8086"\]#\["http://'${influxdb_host}':'${influxdb_port}'"\]#' ${telegraf_conf}
    sed -i 's/database = "telegraf"/database = "'${influxdb_db}'"/' ${telegraf_conf}
    if [[ ! -z ${influxdb_user} ]]; then
        sed -i 's/# username = "telegraf"/username = "'${influxdb_user}'"/' ${telegraf_conf}
    fi
    if [[ ! -z ${influxdb_pwrd} ]]; then
        sed -i 's/# password = "metricsmetricsmetricsmetrics"/password = "'${influxdb_pwrd}'"/' ${telegraf_conf}
    fi

    echo "Enabling telegraf autostart"
    systemctl enable telegraf.service

    echo "Starting telegraf"
    systemctl start telegraf.service

    echo "Removing unused files..."
    cd ..
    rm -f --preserve-root ${telegraf_archive}
    rm -rf telegraf

    echo "Telegraf installed and launched"
fi
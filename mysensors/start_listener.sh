#!/bin/bash

echo "Starting MySPersistor..."
nohup ./myspersistor.sh > /dev/null 2>&1 &
echo "MySPersistor started"

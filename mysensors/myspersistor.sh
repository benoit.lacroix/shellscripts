#!/bin/bash

#################################################
# Title:  MQTT persistor                        #
# Author: Benoît lacroix                        #
# Date:   06/10/2017                            #
#################################################

source myspersistor.conf
source ../lib/lib_logging.sh
source ../lib/lib_xml.sh

# Lib logging configuration
if [[ "${log_verbose}" == "false" ]]; then
    setVerboseOff
fi
setConsoleLogLevel ${log_level_console}
setFileLogLevel ${log_level_file}
setOutputLog ${log_directory}/${log_file_pattern}_$(date +"%Y%m%d_%H%M%S").log
setDieOn

# Lib XML configuration
setAppendDatas false

# MQTT configuration
mqtt_host=${mqtt_host}
mqtt_port=${mqtt_port}
mqtt_topic=${mqtt_topic}
mqttp_ka=${mqtt_keep_alive}
pid_file=${0}.pid

# FiFo queue configuration
if [[ ! -z "${fifo_queue}" ]]; then
    fifo=${fifo_queue}
    LOG_DEBUG "Setting fifo name: ${fifo}"
else
    fifo=mqtt_fifo
    LOG_DEBUG "Setting default fifo name ${fifo}"
fi

# Output configuration
output_workdir=${output_working_directory}
output_donedir=${output_done_directory}
output_pattern=${output_file_pattern}
output_max_size=${output_file_size}

function initiate_output_file {
    output_filename="${output_pattern}_$(date +"%Y%m%d_%H%M%S").xml"
    setOutputFile "${output_workdir}/${output_filename}"
    wHeader
    wStartTag "mySensorsMessages"
}

function write_message {
    LOG_DEBUG "Retrieving information from message"
    local input_message=${1}
    local topic_part="$(echo ${input_message} | cut -d" " -f1)"

    wStartTag "mySensorsMessage"
        wFullTag "date" "$(date +"%d/%m/%Y %H:%M:%S")"
        wFullTag "topic" "$(echo ${topic_part} | cut -d"/" -f1)"
        wFullTag "node" "$(echo ${topic_part} | cut -d"/" -f2)"
        wFullTag "child" "$(echo ${topic_part} | cut -d"/" -f3)"
        wFullTag "command" "$(echo ${topic_part} | cut -d"/" -f4)"
        wFullTag "ack" "$(echo ${topic_part} | cut -d"/" -f5)"
        wFullTag "type" "$(echo ${topic_part} | cut -d"/" -f6)"
        wFullTag "payload" "$(echo ${input_message} | cut -d" " -f2-)"
    wEndTag "mySensorsMessage"
}

function close_output_file {
    wEndTag "mySensorsMessages"
    mv ${output_workdir}/${output_filename} ${output_donedir}/${output_filename}
    cksum "${output_donedir}/${output_filename}" | cut -d" " -f1 > "${output_donedir}/${output_filename}.crc"
}

# Begining of the script
LOG_INFO "Starting MQTT Persistor"

# Checking dependencies
if ! which mosquitto_sub > /dev/null; then
    DIE "Missing mosquitto_sub dependency. Try to install mosquitto-clients package.";
fi

# Fifo for getting incoming messages
LOG_DEBUG "Creating fifo ${fifo}"
mkfifo ${fifo}

LOG_INFO "Suscribing to MQTT queue: ${mqtt_host}:${mqtt_port}@${mqtt_topic} (${mqttp_ka}s)"
mosquitto_sub -h ${mqtt_host} -p ${mqtt_port} -t "${mqtt_topic}" -k ${mqttp_ka} -v > ${fifo} &
pid_mqtt_sub=$!
LOG_INFO "Subscription done (running with pid ${pid_mqtt_sub})"

initiate_output_file
LOG_INFO "New file initiated: ${output_filename}"

messages=0
while true; do
    if read message; then
        LOG_DEBUG "New MQTT message: ${message}"
        if [[ "${message}" == "${stop_msg}" ]]; then
            LOG_INFO "Stopping MQTT subscription"
            kill ${pid_mqtt_sub}
            rm ${fifo}
            LOG_INFO "Closing file: ${output_filename}"
            close_output_file
            LOG_INFO "Exiting MQTT Persistor"
            exit 0
        else
            messages=$((${messages}+1))
            LOG_DEBUG "Preparing message"
            write_message ${message}
            if [[ "${messages}" -ge "${output_max_size}" ]]; then
                LOG_INFO "Max file content reached: ${output_filename}"
                close_output_file
                initiate_output_file
                LOG_INFO "New file initiated: ${output_filename}"
                messages=0
            fi
        fi
    fi
done < ${fifo}

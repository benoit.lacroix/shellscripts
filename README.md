# ShellScripts

This project is for two purposes :
* Providing libraries to do some basic work (i.e. managing logging, writing
xml and so on...)
* Providing fully working project as MySensors metrics saving.

The repository is divided into multiple parts :
* A main directory for all libraries : `lib`
* A directory per project

All library sourcing, must be done like this in the projects files :
```bash
source ../lib_name.sh
```

If needed, subdirectories can be set in the differents projects.

If a library call another library (i.e. `lib_xml` call `lib_logging`), do not
configure `lib_logging` in the `lib_xml` library. Juste use the second library
in the first one. If any configuration is needed, it will be done in the project
that will use the `lib_xml` library. Finally, always source the libraries at the
beginning of your scripts, and before you configure any libraries.
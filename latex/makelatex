#!/bin/bash

# BEGIN HEADER #
script_title="Make LaTeX"
script_version="1.0"
script_date="2019-09-15"
# END HEADER #

function usage {
    echo "Usage:"
    echo "    ${0##*/} -h | -v"
    echo "    ${0##*/} filename [options]"
    echo "With:"
    echo "    -h    Show this help and exit"
    echo "    -v    Show the version and exit"
    echo "Options:"
    echo "    -c    Clean compilation files and exit"
    echo "    -r    Enable a second compilation (for refreshing ToC)"
    echo "    -d    Enable debug mode (no produced file will be removed)"
}

function version {
    echo "${script_title} - v${script_version} (${script_date})"
    pdflatex --version
}

function clean_files {
    echo "Cleaning compilation files"
        rm -f --preserve-root \
            ${working_directory}/${file_name_root}.aux \
            ${working_directory}/${file_name_root}.log \
            ${working_directory}/${file_name_root}.comp.log \
            ${working_directory}/${file_name_root}.synctex.gz \
            ${working_directory}/${file_name_root}.toc
}

if [[ $# -eq 0 ]]; then
    echo "Error: no argument provided"
    usage
    exit 1
fi

case ${1} in
    "-h")
        usage
        exit 0
        ;;
    "-v")
        version
        exit 0
        ;;
    *)
        if [[ ! -f ${1} ]]; then
            echo "File ${1} does not exists"
            usage
            exit 1
        fi
        ;;
esac

shebang_args="${@}"
working_directory=${1%/*}
file_name=${1##*/}
file_name_root=${file_name%.*}
shift

while getopts ":cdrhv" option; do
    case $option in
        "h")
            usage
            exit 0
            ;;
        "v")
            version
            exit 0
            ;;
        "c")
            clean="true"
            ;;
        "d")
            debug="true"
            ;;
        "r")
            recompile="true"
            ;;
        *)
            echo "Unknown option: ${OPTARG}"
            usage
            exit 1
            ;;
    esac
done

# To prevent extra arguments (not starting with dash)
shift $((${OPTIND}-1))
if [[ $# -ne 0 ]]; then
    echo "Unknown argument(s) found: ${@}"
    usage
    exit 1
fi

if [[ "${debug}" == "true" ]]; then
    echo "Shebang: ${0} ${shebang_args}"
fi

if [[ "${clean}" == "true" ]]; then
    clean_files
    exit 0
fi

echo "Replacing shebang line to prevent error during compilation"
sed -i 's/#!/%#!/' ${working_directory}/${file_name}

echo "Compiling LaTeX file (${file_name})"
pdflatex -synctex=1 -interaction=nonstopmode ${working_directory}/${file_name} > ${working_directory}/${file_name_root}.comp.log

if [[ $? -eq 0 ]]; then
    if [[ "${recompile}" == "true" ]]; then
        echo "Second compilation (for refreshing ToC)"
        pdflatex -synctex=1 -interaction=nonstopmode ${working_directory}/${file_name} >> ${working_directory}/${file_name_root}.comp.log
    fi

    if [[ "${debug}" != "true" ]]; then
        clean_files
    fi
else
    echo "Error during compilation (exit code: $?)"
    out=1
fi

echo "Restoring shebang line"
sed -i 's/%#!/#!/' ${working_directory}/${file_name}

exit ${out:-0}
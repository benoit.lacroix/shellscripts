#!/bin/bash

if [[ "$(id -u)" -ne "0" ]]; then
	echo "Script must be run as root"
	exit 1
fi

if [[ ! -e "rundeck.pid" ]]; then
	echo "Rundeck is not running"
else
	cur_pid=$(cat rundeck.pid)
	echo "Stopping Rundeck running with pid $cur_pid"

	kill $cur_pid

	rm rundeck.pid

	if [[ ! -e "rundeck.pid" ]]; then
		echo "Rundeck stopped"
	fi
fi
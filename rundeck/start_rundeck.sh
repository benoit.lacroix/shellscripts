#!/bin/bash

if [[ "$(id -u)" -ne "0" ]]; then
	echo "Script must be run as root"
	exit 1
fi

source rundeck_conf.properties

rundeck_start_log_file=${rdk_start_log_file}
rundeck_pid_file=${rdk_pid_file}

rundeck_start_timeout=${rdk_start_timeout}

rundeck_exec=${rdk_exec}
rundeck_port=${rdk_port}
rundeck_host=${rdk_host}
rundeck_hostname=${rdk_hostname}

if [[ -e "${rundeck_pid_file}" ]]; then
	cur_pid=$(cat ${rundeck_pid_file})
	echo "Rundeck is already running with pid $cur_pid"
	exit 1
fi

echo "Starting Rundeck"

nohup java -Dserver.http.port=${rundeck_port} -Dserver.http.host=${rundeck_host} -Dserver.hostname=${rundeck_hostname} -jar ${rundeck_exec} > ${rundeck_start_log_file} 2>&1 & echo $! > ${rundeck_pid_file}

if [[ -e "${rundeck_pid_file}" ]]; then
	cur_pid=$(cat ${rundeck_pid_file})
	started=0
	SECONDS=0

	while [[ "${started}" -eq "0" ]]; do
		if [[ ${SECONDS} -ge ${rundeck_start_timeout} ]]; then
			kill $cur_pid
			echo "Timeout while starting Rundeck."
			rm ${rundeck_pid_file}
			exit 1
		elif grep -q "ServerConnector:main: Started ServerConnector" ${rundeck_start_log_file}; then
			echo "Rundeck is running with pid $cur_pid"
			echo "Started in $(($SECONDS/60))min $(($SECONDS%60))s"
			started=1
		else
			sleep 1s
		fi
	done
fi

exit 0
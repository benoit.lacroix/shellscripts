# Clean Files library
#
# Author: Benoît Lacroix
# Date: 2017/07/08
# Mail: benoit.lacroix@outlook.com

# Loading logging library
source lib_logging.sh

# Retention value (in minutes)
__prop_retention=0

# Function for setting retention value
function setRetention {
    local tmp_value=${1}
    local tmp_unit=${2}

    case "${tmp_unit}" in
        "W" ) __prop_retention=$((${tmp_value}*7*24*60)) ;;
        "D" ) __prop_retention=$((${tmp_value}*24*60)) ;;
        "H" ) __prop_retention=$((${tmp_value}*60)) ;;
        "M" ) __prop_retention=${tmp_value} ;;
        * )
            LOG_ERROR "Invalid retention unit ${tmp_unit}"
            exit 1
            ;;
    esac
    LOG_DEBUG "Setting retention value to ${__prop_retention} minute(s)"
}

function rmf {
    local file=${1}
    if [[ -e ${file} ]]; then
        rm -f ${file}
        LOG_INFO "File removed: ${file}"
    else
        LOG_ERROR "File does not exists: ${file}"
    fi
}

function findAndRemove {
    local base_dir=${1}
    if [[ -e ${base_dir} ]]; then
        find . -mmin ${__prop_retention} -exec rmf {}
    else
        LOG_ERROR "Unknown directory ${base_dir}"
        return 1
    fi
}
